<?php

class IncomeBudget extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $income_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    public $year;

    /**
     *
     * @var integer
     * @Column(type="integer", length=2, nullable=false)
     */
    public $month;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=false)
     */
    public $budget;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $cleared;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $cleared_date;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'income_budget';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return IncomeBudget[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return IncomeBudget
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
