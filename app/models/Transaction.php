<?php

class Transaction extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $rec_md5;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $trans_date;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $check_number;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $description;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    public $withdrawal;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    public $deposit;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $info;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $info_md5;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    public $budget_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'transaction';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Transaction[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Transaction
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
