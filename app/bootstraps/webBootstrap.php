<?php

define('APPLICATION_INTERFACE', 'WEB');

try {
    require_once('commonBootstrap.php');

    // Init Phalcon AutoLoader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(
        array(
            APPLICATION_PATH . '/controllers/',
            APPLICATION_PATH . '/models/',
            APPLICATION_PATH . '/libraries/'
        )
    );

    $loader->register();

    /**
     * Setup Dependency Injection
     *
     * @link http://docs.phalconphp.com/en/latest/api/Phalcon_DI.html
     */
    $di = new \Phalcon\Di\FactoryDefault();

    // Register Session
    $di->setShared(
        'session', function () {
        $session = new \Phalcon\Session\Adapter\Files();
        $session->start();
        return $session;
    }
    );

    // Register URL
    $di->set(
        'url', function () {
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri('');

        return $url;
    }, true
    );

    // Register View
    $di->set(
        'view', function () {
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir(APPLICATION_PATH . '/views');
        $view->registerEngines(
            array(
                '.phtml' => '\Phalcon\Mvc\View\Engine\Php'
            )
        );
        return $view;
    }, true
    );

    // Register Databases
    $di->setShared(
        'db', function () use ($config) {
        return new \Phalcon\Db\Adapter\Pdo\Mysql(
            array(
                'host'     => $config->database->host,
                'username' => $config->database->username,
                'password' => $config->database->password,
                'dbname'   => $config->database->dbname
            )
        );
    }
    );

    // Register Config
    $di->setShared('config', $config);

    // Register Web Flash Helper Settings
    $di->set(
        'flash', function () {
        $flash = new \Phalcon\Flash\Session(
            array(
                'error'   => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice'  => 'alert alert-info',
                'warning' => 'alert alert-warning',
            )
        );
        return $flash;
    }
    );

    $application = new \Phalcon\Mvc\Application($di);
    echo $application->handle()->getContent();
} catch (\Exception $e) {
    echo "Bootstrap Exception: ", $e->getMessage() . $e->getTraceAsString();
}