<?php

define('APPLICATION_INTERFACE', 'TEST');

require_once ('commonBootstrap.php');

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;

ini_set('display_errors',1);
error_reporting(E_ALL);

// Phalcon AutoLoader
$loader = new \Phalcon\Loader();
$loader->registerDirs(
    array(
        APPLICATION_PATH . '/../test/',
        APPLICATION_PATH . '/tasks/',
        APPLICATION_PATH . '/models/',
        APPLICATION_PATH . '/libraries/'
    )
)->register();


$loader->register();

$di = new FactoryDefault();
Di::reset();

$di = new \Phalcon\DI\FactoryDefault\CLI();

$di->setShared('cache', $cache);

// Registering a dispatcher
$di->setShared(
    'dispatcher', function () use ($di) {
    $dispatcher = new \Phalcon\CLI\Dispatcher;
    $dispatcher->setDI($di);
    return $dispatcher;
}
);

// Register Session
$session = new \Phalcon\Session\Adapter\Files();
$session->start();
$di->setShared('session', $session);

// Register Config Files
$di->setShared('config', $config);

// Database connection is created based in the parameters defined in the configuration file
$di->set(
    'db', function () use ($config) {
    return new \Phalcon\Db\Adapter\Pdo\Mysql(
        array(
            'host'     => $config->database->host,
            'username' => $config->database->username,
            'password' => $config->database->password,
            'dbname'   => $config->database->dbname
        )
    );
}
);

Di::setDefault($di);
