#!/bin/bash

echo 'Compile SASS'
sassc app/assets/scss/base.scss > app/assets/css/app.css

echo 'Concat CSS'
cat \
app/assets/css/bootstrap.min.css \
app/assets/css/bootstrap-theme.min.css \
app/assets/css/font-awesome.min.css \
app/assets/css/app.css \
> public/css/site.min.css


echo 'Concat JS'
cat \
app/assets/js/jquery.min.js \
app/assets/js/bootstrap.min.js \
> public/js/site.min.js
